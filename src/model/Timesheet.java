package model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Time;
import java.util.Date;

@DatabaseTable(tableName = "timesheet")
public class Timesheet extends BaseModel {
    public static int STATUS_NEW = 1;
    public static int STATUS_EDITED = 2;
    public static int STATUS_APPROVED = 4;

    @DatabaseField(foreign = true, columnName = "user_id")
    private User user;

    @DatabaseField(foreign = true, columnName = "project_id")
    private Project project;

    @DatabaseField(foreign = true, columnName = "task_id")
    private Task task;

    @DatabaseField(columnName = "date_submitted", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd")
    private Date dateSubmitted;

    //@DatabaseField(columnName = "time_from", dataType = DataType.SERIALIZABLE)
    //@DatabaseField(columnName = "time_from", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm")
    //private Time timeFrom;

    @DatabaseField(columnName = "time")
    private int time;

    @DatabaseField(columnName = "project_name")
    private String projectName;

    @DatabaseField(columnName = "task_name")
    private String taskName;

    @DatabaseField(columnName = "comment")
    private String comment;

    @DatabaseField(columnName = "status")
    private int status;

    @DatabaseField(columnName = "old_id")
    private long oldId;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getOldId() {
        return oldId;
    }

    public void setOldId(long oldId) {
        this.oldId = oldId;
    }
}
