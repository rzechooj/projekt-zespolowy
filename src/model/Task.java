package model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "tasks")
public class Task extends BaseModel {

    @DatabaseField(columnName = "name")
    private String name;

    @DatabaseField(columnName = "completion")
    private int completion;

    @DatabaseField(columnName = "eta")
    //@DatabaseField(columnName = "eta", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd")
    //private Date eta;
    private int eta;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCompletion() {
        return completion;
    }

    public void setCompletion(int completion) {
        this.completion = completion;
    }

    /*public Date getEta() {
        return eta;
    }

    //public void setEta(Date eta) {
        this.eta = eta;
    }*/

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }
}
