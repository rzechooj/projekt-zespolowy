package model;

import com.j256.ormlite.field.DatabaseField;

public abstract class BaseModel {
    public static final String COLUMN_ID = "id";

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
