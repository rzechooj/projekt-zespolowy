package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Arrays;
import java.util.List;

@DatabaseTable(tableName = "projects")
public class Project extends BaseModel {

    @DatabaseField(columnName = "name")
    private String name;

    @DatabaseField(columnName = "location")
    private String location;

    @DatabaseField(columnName = "managers")
    private String managers;

    @DatabaseField(columnName = "workers")
    private String workers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getManagers() {
        return managers;
    }

    public void setManagers(String managers) {
        this.managers = managers;
    }

    public String getWorkers() {
        return workers;
    }

    public void setWorkers(String workers) {
        this.workers = workers;
    }
}
