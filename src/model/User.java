package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class User extends BaseModel {

    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_EMAIL = "e_mail";
    public static final String COLUMN_ROLE_ID = "users_roles_id";
    public static final String COLUMN_HELP_QUESTION = "help_question";
    public static final String COLUMN_HELP_ANSWER = "help_answer";


    @DatabaseField(columnName = COLUMN_LOGIN)
    private String login;

    @DatabaseField(columnName = COLUMN_PASSWORD)
    private String password;

    @DatabaseField(columnName = COLUMN_FIRST_NAME)
    private String firstName;

    @DatabaseField(columnName = COLUMN_LAST_NAME)
    private String lastName;

    @DatabaseField(columnName = COLUMN_EMAIL)
    private String email;

    @DatabaseField(columnName = COLUMN_ROLE_ID)
    private int usersRolesId;

    @DatabaseField(columnName = COLUMN_HELP_QUESTION)
    private int helpquestion;

    @DatabaseField(columnName = COLUMN_HELP_ANSWER)
    private String helpanswer;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUsersRolesId() {return usersRolesId; }

    public void setUsersRolesId(int usersRolesId) {
        this.usersRolesId = usersRolesId;
    }

    public int getHelpQuestion() {
        return helpquestion;
    }

    public void setHelpQuestion(int helpquestion) {
        this.helpquestion = helpquestion;
    }

    public String getHelpAnswer() {
        return helpanswer;
    }

    public void setHelpAnswer(String helpanswer) {
        this.helpanswer = helpanswer;
    }
}
