package project;

import dao.ProjectDao;
import model.Project;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Łukasz on 10.04.2019.
 */
public class ProjectController {
    ProjectDao projectDao;
    List<Project> projectList;
    Project currentProject;

    public ProjectController(String projectName) throws SQLException {
    projectDao = ProjectDao.getInstance();
    projectList = projectDao.getAll();
    loadProject(projectName);
    if(currentProject!=null)
        System.out.println("Project loaded into controller");
    }
    /*
    loads first occurance of project into variable
     */
    void loadProject(String projectName){
        for(Project p : projectList){
            if(p.getName().equalsIgnoreCase(projectName))
                currentProject = p;
        }
    }

}
