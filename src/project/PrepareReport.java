package project;

import dao.TaskDao;
import dao.TimesheetDao;
import model.Project;
import model.Task;
import model.Timesheet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PrepareReport {
    private List<Timesheet> timesheetList;
    private List<Task> taskList;
    private List<Task> projectTasksList;
    public PrepareReport() throws SQLException {
        timesheetList = TimesheetDao.getInstance().getAll();
        taskList = TaskDao.getInstance().getAll();
        projectTasksList = new ArrayList<>();
    }

    public List<Task> getProjectWithTasks(Project project) {
        for(int i=0;i<timesheetList.size();i++){
            if(timesheetList.get(i).getProjectName().equals(project.getName())){
                for(int j=0;j<taskList.size();j++){
                    if(taskList.get(j).getName().equals(timesheetList.get(i).getTaskName())){
                        if(projectTasksList.size()==0) projectTasksList.add(taskList.get(j));
                        else {
                            if(!projectTasksList.contains(taskList.get(j)))projectTasksList.add(taskList.get(j));
                        }
                    }
                }
            }
        }
        return projectTasksList;
    }
    public double getTaskCompletionPercentage(List<Task> tasklist){
       double sum = 0;
       for(Task t: tasklist){
           sum+=t.getCompletion();
       }
       return sum*1.0/tasklist.size();
    }
    public double getAllTasksEta(List<Task> tasks) {
        double all = 0;
        for(Task t:tasks){
            all = t.getEta();
        }
        return all;
    }
}
