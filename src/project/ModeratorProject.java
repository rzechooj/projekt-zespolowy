package project;

import auth.LoginController;
import dao.TimesheetDao;
import model.Timesheet;
import model.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Łukasz on 20.05.2019.
 */
public class ModeratorProject {
    LoginController loginController;
    ProjectController projectController;
    Timesheet timesheet;
    List<Timesheet> timesheets;
    public ModeratorProject(User user, LoginController loginController) {
        this.loginController = loginController;
        loginController.setUser(user);
    }

     public boolean verifyIsModerator(){
        if(loginController.getAccountRole() == 2){
            return true;
        }
        else {
            System.out.println("Wrong account type - user is not moderator");
            return false;
        }
    }
     public void loadProjects(String projectName, User user) throws SQLException {
        projectController = new ProjectController(projectName);
        timesheets = TimesheetDao.getInstance().getTimesheetsForProjectByName(projectName, user);
    }
     public boolean changeTimesheet(int index){
         try {
             this.timesheet = timesheets.get(index);
             return true;
         } catch (NullPointerException a) {
             System.out.println("No timesheets assigned");
             a.printStackTrace();
             return false;
         } catch (IndexOutOfBoundsException b) {
             System.out.println("No such index for timesheet");
             b.printStackTrace();
             return false;
         }
     }

}
