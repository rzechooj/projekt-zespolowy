package project;


import auth.LoginController;
import dao.TimesheetDao;
import model.Timesheet;
import model.User;

import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by Łukasz on 11.04.2019.
 */
public class WorkerProject {
    public String projectName;
    public User worker;
    public byte accountRole;
    public ProjectController projectController;
    public LoginController loginController;
    List<Timesheet> userTimesheets;
    Timesheet timesheet;

    public WorkerProject(String projectName, LoginController loginController, User user) throws SQLException {
        this.loginController = loginController;
        loginController.setUser(user);
        this.accountRole = loginController.getAccountRole();
        if(checkIsAllowed()) {
            System.out.println("allowed");
            this.projectController = new ProjectController(projectName);
            setProjectName(projectName);
            this.worker = user;
            //userTimesheets = TimesheetDao.getInstance().getTimesheetsForProjectByName(projectName, worker);
            //timesheet = userTimesheets.get(0);
        }
        }


    public boolean changeTimesheet(int index) {
        try {
            this.timesheet = userTimesheets.get(index);
            return true;
        } catch (NullPointerException a) {
            System.out.println("No timesheets assigned");
            a.printStackTrace();
            return false;
        } catch (IndexOutOfBoundsException b) {
            System.out.println("No such index for timesheet");
            b.printStackTrace();
            return false;
        }
    }
    public void addToTimesheet(Time timeFrom, Time timeTo){
        //this.timesheet.setTimeFrom(timeFrom);
        //this.timesheet.setTimeTo(timeTo);
        //this.timesheet.setDateSubmitted(new Date());
    }

    public boolean checkIsAllowed() {
        return accountRole == 1 || accountRole == 2;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName( String projectName) {
    this.projectName = projectName;
    }

    public List<Timesheet> getTimesheetsForProject(String username, String projectName) throws SQLException {
       return TimesheetDao.getInstance().getTimesheetsForProjectByName(projectName, worker);
    }



}
