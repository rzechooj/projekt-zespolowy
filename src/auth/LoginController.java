package auth;


import dao.RoleDao;
import dao.UserDao;
import dao.UserRoleDao;
import model.Role;
import model.User;
import model.UserRole;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Łukasz on 03.04.2019.
 */
public class LoginController {
    private String login;
    private String password;
    public User user;
    public boolean isSessionAlive;
    UserDao userDao;
    RoleDao roleDao;
    UserRoleDao userRoleDao;

    public LoginController() {
        user = new User();
        isSessionAlive = false;
        try {
            userRoleDao = UserRoleDao.getInstance();
            roleDao = RoleDao.getInstance();
            userDao = UserDao.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public byte login() throws SQLException {
        user.setLogin(this.login);
        user.setPassword(this.password);

        if (UserDao.getInstance().findUser(user.getLogin(), user.getPassword())) {
            this.user.setId(userDao.getUserIdFromDB(this.user).get(0).getId());
            setSessionAlive(true);
            return getAccountRole();
        } else {
            System.out.println("Nieprawidlowe haslo lub login");
            return -1;
        }
    }

    public byte getAccountRole() {
        List<UserRole> uRole = null;
        List<Role> roles = null;
        try {
            uRole = UserRoleDao.getInstance().getUserRole(this.user);

            //    role = roleDao.getRolesForUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (uRole.isEmpty()) {
            System.out.println("No role set up for user! error during login");
            return -2;
        } else if (uRole.size() > 1) {
            System.out.println("Multiple roles set up for one user! error during login");
            return -2;
        } else {
            try {
                roles = RoleDao.getInstance().getRolesForUserByID(uRole.get(0).getRole().getId());
            } catch (SQLException e) {
                System.out.println("Error during getting role for account! error during login");
            }
            System.out.println("This user is " + roles.get(0).getName());
            return Byte.valueOf(roles.get(0).getName());

        }
    }


    public String retreivePasswordFromDB(String login) {
        String _password = null;
        if (login != null) {
            try {
                _password = userDao.retrievePasswordFromDB(login);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return _password;
    }

    public int retreiveHelpQuestionFromDB(String login) {
        int _helpquestion = -1;
        if (login != null) {
            try {
                _helpquestion = userDao.retriveHelpQuestionFromDB(login);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return _helpquestion;
    }

    public String retreiveHelpAnswerFromDB(String login) {
        String _helpanswer = null;
        if (login != null) {
            try {
                _helpanswer = userDao.retrieveHelpAnswerFromDB(login);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return _helpanswer;
    }

    public boolean checkUser(String login){
        boolean _checkuser = false;
        if (login != null) {
            try {
                _checkuser = userDao.checkUser(login);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return _checkuser;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {

        return null;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSessionAlive(boolean sessionAlive) {
        isSessionAlive = sessionAlive;
    }

    public boolean isSessionAlive() {
        return isSessionAlive;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
