package gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PasswordChangeDialog {

    public static String display() {
        Label newPassword = new Label();
        newPassword.setText(null);
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Zmiana hasła");
        window.setMinWidth(250);
        window.setResizable(false);

        Label label = new Label();
        label.setText("Proszę podać nowe hasło");

        AnchorPane layout = new AnchorPane();

        VBox vbox = new VBox(10);
        vbox.getChildren().addAll(label);
        vbox.setAlignment(Pos.CENTER);

        PasswordField password = new PasswordField();
        vbox.getChildren().add(password);

        PasswordField secondPassword = new PasswordField();
        vbox.getChildren().add(secondPassword);

        HBox hbox = new HBox(10);

        Button okButton = new Button("Ok");
        okButton.setOnAction(e ->
        {
            if(password.getText().isEmpty() || secondPassword.getText().isEmpty()){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Brak wszystkich informacji");
                alert.setHeaderText(null);
                alert.setContentText("Oba pola muszą być wypełnone");

                alert.showAndWait();
            } else {
                if(password.getText().equals(secondPassword.getText()))
                {
                    newPassword.setText(password.getText());
                    window.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Błąd");
                    alert.setHeaderText(null);
                    alert.setContentText("Oba pola muszą być takie same");

                    alert.showAndWait();
                }
            }
        });

        Button cancelButton = new Button("Anuluj");
        cancelButton.setOnAction(e -> window.close());

        hbox.getChildren().addAll(okButton, cancelButton);
        hbox.setAlignment(Pos.CENTER);
        vbox.getChildren().add(hbox);

        layout.getChildren().addAll(vbox);
        AnchorPane.setTopAnchor(vbox, 10.0);
        AnchorPane.setLeftAnchor(vbox, 10.0);
        AnchorPane.setTopAnchor(vbox, 10.0);
        AnchorPane.setRightAnchor(vbox, 10.0);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return newPassword.getText();
    }
}
