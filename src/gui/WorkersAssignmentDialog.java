package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorkersAssignmentDialog {

    public static String display(List<User> user, String projectWorkersList) {
        List<User> userDBWorkersList = new ArrayList<User>();
        for(int i=0;i<user.size();i++){
            if(user.get(i).getUsersRolesId()==1 || user.get(i).getUsersRolesId()==11 || user.get(i).getUsersRolesId()==21) userDBWorkersList.add(user.get(i));
        }

        List<String> currentWorkerList = new ArrayList<String>();
        List<CheckBox> checkBoxesStatus = new ArrayList<CheckBox>();

        if(projectWorkersList==null) ;
        else if(projectWorkersList.contains(",")) currentWorkerList = Arrays.asList(projectWorkersList.split(","));
        else currentWorkerList.add(projectWorkersList);

        Label workersList = new Label();
        workersList.setText(projectWorkersList);

        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Przypisanie pracowników do projektu");
        window.setResizable(false);

        BorderPane layout = new BorderPane();
        layout.setPrefWidth(300);

        GridPane gridPane = new GridPane();
        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setHalignment(HPos.RIGHT);
        columnConstraints1.setFillWidth(false);
        columnConstraints1.setHgrow(Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10);
        ColumnConstraints columnConstraints2 = new ColumnConstraints();
        columnConstraints2.setHalignment(HPos.LEFT);
        columnConstraints2.setFillWidth(false);
        columnConstraints2.setHgrow(Priority.SOMETIMES);
        columnConstraints2.setMinWidth(10);
        gridPane.getColumnConstraints().addAll(columnConstraints1,columnConstraints2);

        for(int i=0;i<userDBWorkersList.size();i++){
            Label label = new Label();
            label.setText(userDBWorkersList.get(i).getLogin());

            CheckBox checkBox = new CheckBox();
            if(currentWorkerList.contains(userDBWorkersList.get(i).getLogin())) checkBox.setSelected(true);
            checkBox.setId(userDBWorkersList.get(i).getLogin());
            checkBoxesStatus.add(checkBox);

            gridPane.add(label, 0 , i);
            GridPane.setMargin(label, new Insets(5,5,5,5));
            gridPane.add(checkBox, 1, i);
            GridPane.setMargin(checkBox, new Insets(5,5,5,5));
        }

        Button okButton = new Button("Ok");
        okButton.setOnAction(e ->
        {
            String text="";
            for (int i=0;i<checkBoxesStatus.size();i++){
                if(checkBoxesStatus.get(i).isSelected())text+=checkBoxesStatus.get(i).getId()+",";
            }

            if(!text.isEmpty())text = text.substring(0 , text.length() -1);
            else text=null;

            workersList.setText(text);
            window.close();
        });

        Button cancelButton = new Button("Anuluj");
        cancelButton.setOnAction(e -> window.close());

        gridPane.add(okButton,0,userDBWorkersList.size());
        GridPane.setMargin(okButton, new Insets(5,5,5,5));
        gridPane.add(cancelButton,1,userDBWorkersList.size());
        GridPane.setMargin(cancelButton, new Insets(5,5,5,5));

        layout.setCenter(gridPane);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return workersList.getText();
    }
}