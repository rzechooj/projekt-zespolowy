package gui;

import auth.LoginController;
import dao.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import model.*;
import project.PrepareReport;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.toIntExact;

public class Controller implements Initializable {

    public AnchorPane APMain;
    public BorderPane BPLogin;
    public BorderPane BPForgottenPassword;
    public BorderPane BPUser;
    public BorderPane BPManager;
    public BorderPane BPAdmin;
    public BorderPane BPNewAccount;
    public BorderPane BPNewProject;
    public BorderPane BPEditProject;
    public BorderPane BPNewTask;
    public BorderPane BPEditTask;
    public BorderPane BPNewTimesheet;
    public BorderPane BPEditTimesheet;
    public BorderPane BPReport;
    public Button OKLoginButton;
    public Button OKForgottenPasswordButton;
    public Button ForgottenPasswordFormButton;
    public Button addWorkRecordUserButton;
    public Button editWorkRecordUserButton;
    public Button filterWorkRecordsUserButton;
    public Button logoutUserButton;
    public Button logoutManagerButton;
    public Button logoutAdminButton;
    public Button NewAccountLoginButton;
    public Button cancelNewAccountButton;
    public Button OKNewAccountButton;
    public Button banAdminButton;
    public Button forcePasswordChangeAdminButton;
    public Button changeRoleAdminButton;
    public Button addProjectAdminButton;
    public Button editProjectAdminButton;
    public Button deleteProjectAdminButton;
    public Button setManagersAdminButton;
    public Button setWorkersAdminButton;
    public Button newProjectOkAdminButton;
    public Button newProjectCancelAdminButton;
    public Button editProjectOkAdminButton;
    public Button editProjectCancelAdminButton;
    public Button generateReportManagerButtor;
    public Button addTaskManagerButton;
    public Button editTaskManagerButton;
    public Button deleteTaskManagerButton;
    public Button editTaskOkManagerButton;
    public Button editTaskCancelManagerButton;
    public Button newTaskOkManagerButton;
    public Button newTaskCancelManagerButton;
    public Button acceptTimesheetManagerButton;
    public Button dropTimesheetManagerButton;
    public Button newTimesheetOkUserButton;
    public Button newTimesheetCancelUserButton;
    public Button editTimesheetOkUserButton;
    public Button editTimesheetCancelUserButton;
    public Button removeFilterTimesheetUser;
    public Button removeFilterProjectAdmin;
    public Button removeFilterTaskAdmin;
    public Button removeFilterUsersAdmin;
    public Button removeFilterTimesheetManager;
    public Button removeFilterProjectManager;
    public Button removeFilterTaskManager;
    public TextField TFLogin;
    public TextField TFForgottenPasswordFormLogin;
    public TextField answerNewAccountTF;
    public TextField emailNewAccountTF;
    public TextField surnameNewAccountTF;
    public TextField nameNewAccountTF;
    public TextField loginNewAccountTF;
    public TextField newProjectNameAdminTF;
    public TextField newProjectLocationAdminTF;
    public TextField editProjectNameAdminTF;
    public TextField editProjectLocationAdminTF;
    public TextField newTaskNameManagerTF;
    public TextField newTaskCompletionManagerTF;
    public TextField editTaskNameManagerTF;
    public TextField editTaskCompletionManagerTF;
    public TextField newTimesheetTimeUserTF;
    public TextField newTimesheetCommentUserTF;
    public TextField editTimesheetTimeUserTF;
    public TextField editTimesheetCommentUserTF;
    public TextField newTaskEtaManagerTF;
    public TextField editTaskEtaManagerTF;
    public TextField filterTimesheetUser;
    public TextField filterTaskManager;
    public TextField filterProjectManager;
    public TextField filterTimesheetManager;
    public TextField filterUsersAdmin;
    public TextField filterTaskAdmin;
    public TextField filterProjectAdmin;
    public PasswordField PFPassword;
    public PasswordField secondPasswordNewAccountPF;
    public PasswordField passwordNewAccountPF;
    public TableView workRecordUserTableView;
    public TableView userListTableView;
    public TableView projectsListTableView;
    public TableView projectsListManagerTableView;
    public TableView taskListAdminTableView;
    public TableView taskListManagerTableView;
    public TableView workRecordManagerTableView;
    public ComboBox questionsNewAccountCB;
    public ComboBox newTimesheetProjectUserCB;
    public ComboBox newTimesheetTaskUserCB;
    public ComboBox editTimesheetProjectUserCB;
    public ComboBox editTimesheetTaskUserCB;
    public TableColumn loginAdminUserListColumn;
    public TableColumn nameAdminUserListColumn;
    public TableColumn surnameAdminUserListColumn;
    public TableColumn emailAdminUserListColumn;
    public TableColumn roleAdminUserListColumn;
    public TableColumn projectNameAdminProjectListColumn;
    public TableColumn projectLocationAdminProjectListColumn;
    public TableColumn projectManagersAdminProjectListColumn;
    public TableColumn projectWorkersAdminProjectListColumn;
    public TableColumn projectNameManagerProjectListColumn;
    public TableColumn projectLocationManagerProjectListColumn;
    public TableColumn projectWorkersManagerProjectListColumn;
    public TableColumn taskNameAdminTaskListColumn;
    public TableColumn taskCompletionAdminTaskListColumn;
    public TableColumn taskEtaAdminTaskListColumn;
    public TableColumn taskNameManagerTaskListColumn;
    public TableColumn taskCompletionManagerTaskListColumn;
    public TableColumn taskEtaManagerTaskListColumn;
    public TableColumn dateTimesheetUserColumn;
    public TableColumn timeTimesheetUserColumn;
    public TableColumn projectTimesheetUserColumn;
    public TableColumn taskTimesheetUserColumn;
    public TableColumn commentTimesheetUserColumn;
    public TableColumn dateTimesheetManagerColumn;
    public TableColumn timeTimesheetManagerColumn;
    public TableColumn projectTimesheetManagerColumn;
    public TableColumn taskTimesheetManagerColumn;
    public TableColumn commentTimesheetManagerColumn;
    public TableColumn statusTimesheetManagerColumn;
    public TableColumn statusTimesheetUserColumn;
    public DatePicker newTaskEtaManagerDP;
    public DatePicker editTaskEtaManagerDP;
    public Button closeReportButton;
    public PieChart ReportPieChart;
    public TextField ReportProjectName;
    public TextField ReportRemainingTime;
    public TextField ReportTaskName;
    public Label ReportTaskDisplayCounter;
    public TableView ReportTaskTableView;
    public TableColumn ReportTaskColumn;
    public ProgressBar ReportProgressBar;
    public Label ReportProgressText;
    private String emailRegex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    private Pattern emailPattern;
    private Matcher emailMatcher;
    private String[] helpQuestions = {"Czego boisz się najbardziej?", "Największe marzenie Twojego życia?", "Co lubisz najbardziej?",
            "Co widzisz za oknem domu?", "Czego nie lubisz najbardziej?", "Co robisz najlepiej?" };
    private String[] roleList = {"Pracownik", "Manager", "Administrator" };
    private ObservableList<Timesheet> timesheetListUser;
    private ObservableList<Timesheet> timesheetListManager;
    private ObservableList<User> userList;
    private ObservableList<Task> taskListAdmin;
    private ObservableList<Task> taskListManager;
    private ObservableList<Project> projectListAdmin;
    private ObservableList<Project> projectListManager;
    private ObservableList<Task> oTasks;
    private byte accountRole;
    private LoginController loginController;
    private Project project;
    private Task task;
    private Timesheet timesheet;
    private List<Project> projectList;
    private List<Task> taskList;
    private SortedList<Timesheet> timesheetListUserSortedData;
    private SortedList<Timesheet> timesheetListManagerSortedData;
    private SortedList<User> userListSortedData;
    private SortedList<Task> taskListAdminSortedData;
    private SortedList<Task> taskListManagerSortedData;
    private SortedList<Project> projectListAdminSortedData;
    private SortedList<Project> projectListManagerSortedData;

    //private int debugPanelVal;

    //Metody dla okna logowania

    public void OKLoginButtonHnd(ActionEvent actionEvent) {
        if (TFLogin.getText().isEmpty()) alert("Brak loginu", "Pole login nie może być puste", "warning");
        else loginController.setLogin(TFLogin.getText());

        if (PFPassword.getText().isEmpty()) alert("Brak hasła", "Pole hasła nie może być puste", "warning");
        else loginController.setPassword(MD5(PFPassword.getText()));

        if (!TFLogin.getText().isEmpty() && !PFPassword.getText().isEmpty()) {
            try {
                accountRole = loginController.login();
                if(accountRole==1) {
                    BPLogin.setDisable(true);
                    BPLogin.setVisible(false);
                    BPUser.setDisable(false);
                    BPUser.setVisible(true);
                    populateUserTableViews();
                } else if(accountRole==2) {
                    BPLogin.setDisable(true);
                    BPLogin.setVisible(false);
                    BPManager.setDisable(false);
                    BPManager.setVisible(true);
                    populateManagerTableViews(loginController.getUser().getLogin());
                } else if(accountRole==3) {
                    BPLogin.setDisable(true);
                    BPLogin.setVisible(false);
                    BPAdmin.setDisable(false);
                    BPAdmin.setVisible(true);
                    populateAdminTableViews();
                } else if(accountRole==11 || accountRole==12 || accountRole==13) {
                    alert("Blad", "Konto zostało zablokowane przez administratora", "information");
                } else if(accountRole==21 || accountRole==22 || accountRole==23) {
                    alert("Wymagana zmiana hasła", "Administrator systemu wymaga zmiany hasła dla tego konta", "information");
                    changePasswordDialog(loginController.user.getLogin());
                } else if(accountRole==-1) {
                    alert("Blad", "Nieprawidlowe haslo lub login", "error");
                } else if(accountRole==-2) {
                    alert("Blad", "Uzytkownik nie ma przypisanej roli w systemie", "error");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void OKForgottenPasswordButtonHnd(ActionEvent actionEvent) {
        BPLogin.setDisable(true);
        BPLogin.setVisible(false);
        BPForgottenPassword.setDisable(false);
        BPForgottenPassword.setVisible(true);
    }

    public void NewAccountLoginButtonHnd(ActionEvent actionEvent) {
        BPLogin.setDisable(true);
        BPLogin.setVisible(false);
        BPNewAccount.setDisable(false);
        BPNewAccount.setVisible(true);
    }

    //metody dla okna przypomnienia hasla

    public void ForgottenPasswordFormButtonHnd(ActionEvent actionEvent) throws SQLException{
        if (TFForgottenPasswordFormLogin.getText().isEmpty())
            alert("Brak loginu", "Pole login nie może być puste", "warning");
        if (!TFForgottenPasswordFormLogin.getText().isEmpty()) {

            if (loginController.checkUser(TFForgottenPasswordFormLogin.getText())) {

                TextInputDialog dialog = new TextInputDialog("");
                dialog.setTitle("Odpowiedz na pytanie");
                dialog.setHeaderText(null);
                dialog.setContentText(helpQuestions[loginController.retreiveHelpQuestionFromDB(TFForgottenPasswordFormLogin.getText())]);

                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()) {
                    if (result.get().equals(loginController.retreiveHelpAnswerFromDB(TFForgottenPasswordFormLogin.getText()))) changePasswordDialog(TFForgottenPasswordFormLogin.getText());
                        //alert("Przywrócenie hasła", "Twoje hasło to: " + loginController.retreivePasswordFromDB(TFForgottenPasswordFormLogin.getText()), "information");
                    else alert("Błędna odpowiedź", "Podano błędną odpowiedz do pytania", "error");
                }

                //String password = loginController.retreivePasswordFromDB(TFForgottenPasswordFormLogin.getText());
                //alert("Przywrócenie hasła", "Twoje hasło to: " + password, "information");

                BPForgottenPassword.setDisable(true);
                BPForgottenPassword.setVisible(false);
                BPLogin.setDisable(false);
                BPLogin.setVisible(true);
                TFForgottenPasswordFormLogin.clear();
            } else alert("Błędny login", "Nie znaleziono podanego loginu w bazie", "error");
        }
    }

    //metody dla nowego konta

    public void OKNewAccountButtonHnd(ActionEvent actionEvent) throws SQLException {
        if(loginNewAccountTF.getText().isEmpty() || passwordNewAccountPF.getText().isEmpty() || secondPasswordNewAccountPF.getText().isEmpty() ||
        nameNewAccountTF.getText().isEmpty() || surnameNewAccountTF.getText().isEmpty() || emailNewAccountTF.getText().isEmpty() ||
        questionsNewAccountCB.getSelectionModel().getSelectedIndex()==-1 || answerNewAccountTF.getText().isEmpty()) alert("Brak wymaganych informacji", "Wszystkie pola muszą być wypełnone", "warning");
        else {
            if (passwordNewAccountPF.getText().equals(secondPasswordNewAccountPF.getText())) {
                emailPattern = Pattern.compile(emailRegex);
                emailMatcher = emailPattern.matcher(emailNewAccountTF.getText());
                if (emailMatcher.matches()) {
                    User user = new User();
                    user.setLogin(loginNewAccountTF.getText());
                    user.setPassword(MD5(passwordNewAccountPF.getText()));
                    user.setFirstName(nameNewAccountTF.getText());
                    user.setLastName(surnameNewAccountTF.getText());
                    user.setEmail(emailNewAccountTF.getText());
                    user.setUsersRolesId(1);
                    user.setHelpQuestion(questionsNewAccountCB.getSelectionModel().getSelectedIndex());
                    user.setHelpAnswer(answerNewAccountTF.getText());
                    UserDao.getInstance().add(user);
                    UserRole userRole = new UserRole();
                    userRole.setUser(user);
                    userRole.setRole(RoleDao.getInstance().getObjectById(1));
                    UserRoleDao.getInstance().add(userRole);
                    alert("Konto utworzone", "Zostało utworzone nowe konto", "");
                    BPNewAccount.setDisable(true);
                    BPNewAccount.setVisible(false);
                    BPLogin.setDisable(false);
                    BPLogin.setVisible(true);
                    loginNewAccountTF.clear();
                    passwordNewAccountPF.clear();
                    secondPasswordNewAccountPF.clear();
                    nameNewAccountTF.clear();
                    surnameNewAccountTF.clear();
                    emailNewAccountTF.clear();
                    questionsNewAccountCB.getSelectionModel().clearSelection();
                    answerNewAccountTF.clear();
                } else alert("Niepoprawny e-mail", "Wpisany e-mail jest nieprawidłowy", "warning");
            } else alert("Niepoprawne hasła", "Hasła wpisane w obu polach muszą być identyczne", "warning");
        }
    }

    public void cancelNewAccountButtonHnd(ActionEvent actionEvent) {
        BPNewAccount.setDisable(true);
        BPNewAccount.setVisible(false);
        BPLogin.setDisable(false);
        BPLogin.setVisible(true);
        loginNewAccountTF.clear();
        passwordNewAccountPF.clear();
        secondPasswordNewAccountPF.clear();
        nameNewAccountTF.clear();
        surnameNewAccountTF.clear();
        emailNewAccountTF.clear();
        questionsNewAccountCB.getSelectionModel().clearSelection();
        answerNewAccountTF.clear();
    }

    //metody zarzadzania wpisami rejestru czasu dla uzytkownika

    public void addWorkRecordUserButton(ActionEvent actionEvent) throws SQLException{
        projectList = ProjectDao.getInstance().getAll();
        taskList = TaskDao.getInstance().getAll();
        String[] projectsNames = new String[projectList.size()];
        String[] tasksNames = new String[taskList.size()];
        for(int i=0;i<projectList.size();i++){
            projectsNames[i] = projectList.get(i).getName();
        }
        for(int i=0;i<taskList.size();i++){
            tasksNames[i] = taskList.get(i).getName();
        }
        newTimesheetProjectUserCB.getItems().clear();
        newTimesheetTaskUserCB.getItems().clear();
        newTimesheetProjectUserCB.getItems().addAll(projectsNames);
        newTimesheetTaskUserCB.getItems().addAll(tasksNames);
        BPUser.setDisable(true);
        BPUser.setVisible(false);
        BPNewTimesheet.setDisable(false);
        BPNewTimesheet.setVisible(true);
    }

    public void editWorkRecordUserButton(ActionEvent actionEvent) throws SQLException{
        projectList = ProjectDao.getInstance().getAll();
        taskList = TaskDao.getInstance().getAll();
        String[] projectsNames = new String[projectList.size()];
        String[] tasksNames = new String[taskList.size()];
        for(int i=0;i<projectList.size();i++){
            projectsNames[i] = projectList.get(i).getName();
        }
        for(int i=0;i<taskList.size();i++){
            tasksNames[i] = taskList.get(i).getName();
        }

        if(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getStatus()==0) {

            editTimesheetProjectUserCB.getItems().clear();
            editTimesheetTaskUserCB.getItems().clear();
            editTimesheetProjectUserCB.getItems().addAll(projectsNames);
            editTimesheetTaskUserCB.getItems().addAll(tasksNames);

            editTimesheetTimeUserTF.setText(Integer.toString(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getTime()));
            editTimesheetProjectUserCB.getSelectionModel().select(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getProjectName());
            editTimesheetTaskUserCB.getSelectionModel().select(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getTaskName());
            editTimesheetCommentUserTF.setText(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getComment());

            timesheet = TimesheetDao.getInstance().getObjectById(timesheetListUserSortedData.get(workRecordUserTableView.getSelectionModel().getSelectedIndex()).getId());
            BPUser.setDisable(true);
            BPUser.setVisible(false);
            BPEditTimesheet.setDisable(false);
            BPEditTimesheet.setVisible(true);
        } else alert("Niedozwolona operacja", "Wybrany wpis czeka na akceptacje i nie można go edytowac","");
    }

    public void newTimesheetOkUserButtonHnd(ActionEvent actionEvent) throws SQLException {
        if(isNumeric(editTimesheetTimeUserTF.getText())) {
            if (newTimesheetTimeUserTF.getText().isEmpty() || newTimesheetProjectUserCB.getSelectionModel().getSelectedIndex() == -1 || newTimesheetTaskUserCB.getSelectionModel().getSelectedIndex() == -1)
                alert("Brak wszystkich danych", "Wszystkie pola muszą być wypełnione", "warning");
            else {
                Timesheet timesheet = new Timesheet();
                timesheet.setUser(loginController.getUser());
                Date date = new Date();
                timesheet.setDateSubmitted(date);
                timesheet.setTime(Integer.parseInt(newTimesheetTimeUserTF.getText()));
                for (int i = 0; i < projectList.size(); i++) {
                    if (projectList.get(i).getName().equals(newTimesheetProjectUserCB.getSelectionModel().getSelectedItem().toString()))
                        timesheet.setProject(projectList.get(i));
                }
                for (int i = 0; i < taskList.size(); i++) {
                    if (taskList.get(i).getName().equals(newTimesheetTaskUserCB.getSelectionModel().getSelectedItem().toString()))
                        timesheet.setTask(taskList.get(i));
                }
                timesheet.setProjectName(newTimesheetProjectUserCB.getSelectionModel().getSelectedItem().toString());
                timesheet.setTaskName(newTimesheetTaskUserCB.getSelectionModel().getSelectedItem().toString());
                if (newTimesheetCommentUserTF.getText().isEmpty()) timesheet.setComment("Brak");
                else timesheet.setComment(newTimesheetCommentUserTF.getText());
                timesheet.setStatus(1);
                TimesheetDao.getInstance().add(timesheet);

                timesheetListUser.add(timesheet);
                workRecordUserTableView.refresh();

                filterTimesheetUser();

                BPNewTimesheet.setDisable(true);
                BPNewTimesheet.setVisible(false);
                BPUser.setDisable(false);
                BPUser.setVisible(true);

                newTimesheetTimeUserTF.clear();
                newTimesheetProjectUserCB.getSelectionModel().clearSelection();
                newTimesheetTaskUserCB.getSelectionModel().clearSelection();
                newTimesheetCommentUserTF.clear();
            }
        } else alert("Błędne dane","W polu \"Czas pracy\" powinna być liczba","warning");
    }

    public void newTimesheetCancelUserButtonHnd(ActionEvent actionEvent) {
        BPNewTimesheet.setDisable(true);
        BPNewTimesheet.setVisible(false);
        BPUser.setDisable(false);
        BPUser.setVisible(true);
        newTimesheetTimeUserTF.clear();
        newTimesheetProjectUserCB.getSelectionModel().clearSelection();
        newTimesheetTaskUserCB.getSelectionModel().clearSelection();
        newTimesheetCommentUserTF.clear();
    }

    public void editTimesheetOkUserButtonHnd(ActionEvent actionEvent) throws SQLException{
        if(isNumeric(editTimesheetTimeUserTF.getText())) {
            if (editTimesheetTimeUserTF.getText().isEmpty() || editTimesheetProjectUserCB.getSelectionModel().getSelectedIndex() == -1 || editTimesheetTaskUserCB.getSelectionModel().getSelectedIndex() == -1)
                alert("Brak wszystkich danych", "Wszystkie pola muszą być wypełnione", "warning");
            else {
                Timesheet editTimesheet = new Timesheet();
                editTimesheet.setUser(loginController.getUser());
                Date date = new Date();
                editTimesheet.setDateSubmitted(date);
                editTimesheet.setTime(Integer.parseInt(editTimesheetTimeUserTF.getText()));
                for (int i = 0; i < projectList.size(); i++) {
                    if (projectList.get(i).getName().equals(editTimesheetProjectUserCB.getSelectionModel().getSelectedItem().toString()))
                        editTimesheet.setProject(projectList.get(i));
                }
                for (int i = 0; i < taskList.size(); i++) {
                    if (taskList.get(i).getName().equals(editTimesheetTaskUserCB.getSelectionModel().getSelectedItem().toString()))
                        editTimesheet.setTask(taskList.get(i));
                }
                editTimesheet.setProjectName(editTimesheetProjectUserCB.getSelectionModel().getSelectedItem().toString());
                editTimesheet.setTaskName(editTimesheetTaskUserCB.getSelectionModel().getSelectedItem().toString());
                if (editTimesheetCommentUserTF.getText().isEmpty()) editTimesheet.setComment("Brak");
                else editTimesheet.setComment(editTimesheetCommentUserTF.getText());
                editTimesheet.setOldId(timesheet.getId());
                editTimesheet.setStatus(2);
                TimesheetDao.getInstance().add(editTimesheet);
                timesheetListUser.add(editTimesheet);
                workRecordUserTableView.refresh();

                filterTimesheetUser();

                timesheet = null;

                BPEditTimesheet.setDisable(true);
                BPEditTimesheet.setVisible(false);
                BPUser.setDisable(false);
                BPUser.setVisible(true);

                editTimesheetTimeUserTF.clear();
                editTimesheetProjectUserCB.getSelectionModel().clearSelection();
                editTimesheetTaskUserCB.getSelectionModel().clearSelection();
                editTimesheetCommentUserTF.clear();
            }
        } else alert("Błędne dane","W polu \"Czas pracy\" powinna być liczba","warning");
    }

    public void editTimesheetCancelUserButtonHnd(ActionEvent actionEvent) {
        BPEditTimesheet.setDisable(true);
        BPEditTimesheet.setVisible(false);
        BPUser.setDisable(false);
        BPUser.setVisible(true);

        editTimesheetTimeUserTF.clear();
        editTimesheetProjectUserCB.getSelectionModel().clearSelection();
        editTimesheetTaskUserCB.getSelectionModel().clearSelection();
        editTimesheetCommentUserTF.clear();
    }

    //metody do zarządzania filtrami tabel

    public void removeFilterTimesheetUserHnd(ActionEvent actionEvent) {
        filterTimesheetUser.clear();
    }

    public void removeFilterTaskManagerHnd(ActionEvent actionEvent) {
        filterTaskManager.clear();
    }

    public void removeFilterProjectManagerHnd(ActionEvent actionEvent) {
        filterProjectManager.clear();
    }

    public void removeFilterTimesheetManagerHnd(ActionEvent actionEvent) {
        filterTimesheetManager.clear();
    }

    public void removeFilterUsersAdminHnd(ActionEvent actionEvent) {
        filterUsersAdmin.clear();
    }

    public void removeFilterTaskAdminHnd(ActionEvent actionEvent) {
        filterTaskAdmin.clear();
    }

    public void removeFilterProjectAdminHnd(ActionEvent actionEvent) {
        filterProjectAdmin.clear();
    }

    //metody do wylogowywania uzytkownikow

    public void logoutUserButtonHnd(ActionEvent actionEvent) {
        BPUser.setDisable(true);
        BPUser.setVisible(false);
        BPLogin.setDisable(false);
        BPLogin.setVisible(true);
    }

    public void logoutManagerButtonHnd(ActionEvent actionEvent) {
        BPManager.setDisable(true);
        BPManager.setVisible(false);
        BPLogin.setDisable(false);
        BPLogin.setVisible(true);
    }

    public void logoutAdminButtonHnd(ActionEvent actionEvent) {
        BPAdmin.setDisable(true);
        BPAdmin.setVisible(false);
        BPLogin.setDisable(false);
        BPLogin.setVisible(true);
    }

    //metody do zarzadzania zadaniami dla menadzera

    public void addTaskManagerButtonHnd(ActionEvent actionEvent) {
        BPManager.setDisable(true);
        BPManager.setVisible(false);
        BPNewTask.setDisable(false);
        BPNewTask.setVisible(true);
    }

    public void editTaskManagerButtonHnd(ActionEvent actionEvent) throws SQLException{
        BPManager.setDisable(true);
        BPManager.setVisible(false);
        BPEditTask.setDisable(false);
        BPEditTask.setVisible(true);
        editTaskNameManagerTF.setText(taskListManagerSortedData.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getName());
        editTaskCompletionManagerTF.setText(Integer.toString(taskListManagerSortedData.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getCompletion()));
        //java.util.Date date = taskListManager.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getEta();
        //editTaskEtaManagerDP.setValue(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        editTaskEtaManagerTF.setText(Integer.toString(taskListManagerSortedData.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getEta()));
        task = TaskDao.getInstance().getObjectById(taskListManagerSortedData.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getId());

    }

    public void deleteTaskManagerButtonHnd(ActionEvent actionEvent) throws SQLException{
        long taskId = taskListManagerSortedData.get(taskListManagerTableView.getSelectionModel().getSelectedIndex()).getId();
        taskListManager.remove(taskListManagerTableView.getSelectionModel().getSelectedItem());
        Task task = TaskDao.getInstance().getObjectById(taskId);
        TaskDao.getInstance().remove(task);
        taskListManagerTableView.refresh();
    }

    public void newTaskOkManagerButtonHnd(ActionEvent actionEvent) throws SQLException{
        //newTaskEtaManagerDP.getValue()==null
        if(isNumeric(newTaskCompletionManagerTF.getText()) && isNumeric(newTaskEtaManagerTF.getText())) {
            if(newTaskNameManagerTF.getText().isEmpty() || newTaskCompletionManagerTF.getText().isEmpty() || newTaskEtaManagerTF.getText().isEmpty()) alert("Brak wszystkich danych","Wszystkie pola muszą być wypełnione","warning");
            else {
                Task task = new Task();
                task.setName(newTaskNameManagerTF.getText());
                task.setCompletion(Integer.parseInt(newTaskCompletionManagerTF.getText()));
                //task.setEta(java.sql.Date.valueOf(newTaskEtaManagerDP.getValue()));
                task.setEta(Integer.parseInt(newTaskEtaManagerTF.getText()));
                TaskDao.getInstance().add(task);
                taskListManager.add(task);
                taskListManagerTableView.refresh();
                BPNewTask.setDisable(true);
                BPNewTask.setVisible(false);
                BPManager.setDisable(false);
                BPManager.setVisible(true);
                newTaskNameManagerTF.clear();
                newTaskCompletionManagerTF.clear();
                newTaskEtaManagerDP.setValue(null);
                newTaskEtaManagerTF.clear();
            }
        } else if(isNumeric(newTaskCompletionManagerTF.getText())) alert("Błędne dane","W polu \"Przewidywany czas zakończenia\" powinna być liczba ","warning");
        else if(isNumeric(newTaskEtaManagerTF.getText())) alert("Błędne dane","W polu \"% wykonania zadania\" powinna być liczba","warning");
        else alert("Błędne dane","W polu \"% wykonania zadania\" oraz \"Przewidywany czas zakończenia\" powinny być liczby","warning");
    }

    public void newTaskCancelManagerButtonHnd(ActionEvent actionEvent) {
        BPNewTask.setDisable(true);
        BPNewTask.setVisible(false);
        BPManager.setDisable(false);
        BPManager.setVisible(true);
        newTaskNameManagerTF.clear();
        newTaskCompletionManagerTF.clear();
        newTaskEtaManagerDP.setValue(null);
        newTaskEtaManagerTF.clear();
    }

    public void editTaskOkManagerButtonHnd(ActionEvent actionEvent) throws SQLException {
        //editTaskEtaManagerDP.getValue()==null
        if(isNumeric(editTaskCompletionManagerTF.getText()) && isNumeric(editTaskEtaManagerTF.getText())) {
            if (editTaskNameManagerTF.getText().isEmpty() || editTaskCompletionManagerTF.getText().isEmpty() || editTaskEtaManagerTF.getText().isEmpty())
                alert("Brak wszystkich danych", "Wszystkie pola muszą być wypełnione", "warning");
            else {
                task.setName(editTaskNameManagerTF.getText());
                task.setCompletion(Integer.parseInt(editTaskCompletionManagerTF.getText()));
                //task.setEta(java.sql.Date.valueOf(editTaskEtaManagerDP.getValue()));
                task.setEta(Integer.parseInt(editTaskEtaManagerTF.getText()));
                for (int i = 0; i < taskListManager.size(); i++) {
                    if (taskListManager.get(i).getId() == task.getId()) {
                        taskListManager.get(i).setName(editTaskNameManagerTF.getText());
                        taskListManager.get(i).setCompletion(Integer.parseInt(editTaskCompletionManagerTF.getText()));
                        //taskListManager.get(i).setEta(java.sql.Date.valueOf(editTaskEtaManagerDP.getValue()));
                        taskListManager.get(i).setEta(Integer.parseInt(editTaskEtaManagerTF.getText()));
                        taskListManagerTableView.refresh();
                    }
                }
                TaskDao.getInstance().update(task);
                task = null;
                editTaskNameManagerTF.clear();
                editTaskCompletionManagerTF.clear();
                editTaskEtaManagerDP.setValue(null);
                editTaskEtaManagerTF.clear();
                BPEditTask.setDisable(true);
                BPEditTask.setVisible(false);
                BPManager.setDisable(false);
                BPManager.setVisible(true);
            }
        } else if(isNumeric(editTaskCompletionManagerTF.getText())) alert("Błędne dane","W polu \"Przewidywany czas zakończenia\" powinna być liczba ","warning");
        else if(isNumeric(editTaskEtaManagerTF.getText())) alert("Błędne dane","W polu \"% wykonania zadania\" powinna być liczba","warning");
        else alert("Błędne dane","W polu \"% wykonania zadania\" oraz \"Przewidywany czas zakończenia\" powinny być liczby","warning");
    }

    public void editTaskCancelManagerButtonHnd(ActionEvent actionEvent) {
        BPEditTask.setDisable(true);
        BPEditTask.setVisible(false);
        BPManager.setDisable(false);
        BPManager.setVisible(true);
        task = null;
        editTaskNameManagerTF.clear();
        editTaskCompletionManagerTF.clear();
        editTaskEtaManagerDP.setValue(null);
        editTaskEtaManagerTF.clear();
    }

    public void generateReportManagerButtorHnd(ActionEvent actionEvent) throws SQLException {
        PrepareReport report = new PrepareReport();

        List<Task> tasklist = report.getProjectWithTasks(projectListManager.get(projectsListManagerTableView.getSelectionModel().getFocusedIndex()));
        if (tasklist == null) {
            new Alert(Alert.AlertType.ERROR, "Nie wybrano żadnego projektu!", ButtonType.OK);
        } else {
            ReportProgressText.setText("");
            ReportProgressBar.setProgress(0.0);
            ReportProjectName.clear();
            ReportProjectName.setText(projectListManager.get(projectsListManagerTableView.getSelectionModel().getFocusedIndex()).getName());
            ReportTaskName.clear();
            ReportRemainingTime.clear();

            String css = this.getClass().getResource("/gui/assets/report.css").toExternalForm();
            ReportTaskName.getStylesheets().add(css);
            ReportProjectName.getStylesheets().add(css);
            ReportRemainingTime.getStylesheets().add(css);

            ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                    new PieChart.Data("Gotowe: "+report.getTaskCompletionPercentage(tasklist)+"%", report.getTaskCompletionPercentage(tasklist)),
                    new PieChart.Data("Pozostało", 100));
            ReportPieChart.setData(pieChartData);
            BPManager.setDisable(true);
            BPManager.setVisible(false);
            BPReport.setDisable(false);
            BPReport.setVisible(true);

            ObservableList<Task> taskObservableList = FXCollections.observableArrayList(tasklist);
            ReportTaskColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
            ReportTaskTableView.setItems(taskObservableList);
            ReportTaskTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Task>() {

                @Override
                public void changed(ObservableValue<? extends Task> observable, Task oldValue, Task newValue) {
                    if (newValue != null) {
                        ReportTaskName.setText(newValue.getName());
                        ReportRemainingTime.setText(newValue.getEta() - (newValue.getEta() * (newValue.getCompletion() / 100.0)) + " godzin");
                        ReportProgressBar.setProgress(newValue.getCompletion() / 100.0);
                        ReportProgressText.setText(newValue.getCompletion()+" %");
                    } else
                        new Alert(Alert.AlertType.ERROR, "Proszę wybrać zadanie!", ButtonType.OK);
                }
            });
        }
    }



    public void closeReportButtonHnd(ActionEvent actionEvent){
        BPReport.setDisable(true);
        BPReport.setVisible(false);
        BPManager.setDisable(false);
        BPManager.setVisible(true);

    }
    //metody do zarzadzania wpisami czasu pracy dla menadzera

    public void acceptTimesheetManagerButtonHnd(ActionEvent actionEvent) throws SQLException{
        long timesheetId = timesheetListManagerSortedData.get(workRecordManagerTableView.getSelectionModel().getSelectedIndex()).getId();
        timesheetListManager.remove(workRecordManagerTableView.getSelectionModel().getSelectedItem());
        Timesheet timesheet = TimesheetDao.getInstance().getObjectById(timesheetId);
        Task task = TaskDao.getInstance().getObjectById(timesheet.getTask().getId());
        long percent = task.getCompletion() + Math.round(((double)timesheet.getTime()*100)/task.getEta());
        if(timesheet.getStatus()==1) {
            timesheet.setStatus(0);
            TimesheetDao.getInstance().update(timesheet);
            workRecordManagerTableView.refresh();
            //filteredTimesheetManager();
        } else if(timesheet.getStatus()==2 ){
            List<Timesheet> timesheetList = TimesheetDao.getInstance().getAll();
            for(int i=0;i<timesheetList.size();i++){
                if(timesheetList.get(i).getId()==timesheet.getOldId()){
                    Timesheet tempTimesheet = timesheetList.get(i);
                    TimesheetDao.getInstance().remove(tempTimesheet);
                }
            }
            timesheet.setStatus(0);
            TimesheetDao.getInstance().update(timesheet);
            workRecordManagerTableView.refresh();
            //filteredTimesheetManager();
        }
        task.setCompletion(toIntExact(percent));
        TaskDao.getInstance().update(task);
        for(int i=0;i<taskListManager.size();i++){
            if(taskListManager.get(i).getId()==task.getId()) taskListManager.get(i).setCompletion(toIntExact(percent));

        }
        taskListManagerTableView.refresh();
    }

    public void dropTimesheetManagerButtonHnd(ActionEvent actionEvent) throws SQLException{
        long timesheetId = timesheetListManagerSortedData.get(workRecordManagerTableView.getSelectionModel().getSelectedIndex()).getId();
        timesheetListManager.remove(workRecordManagerTableView.getSelectionModel().getSelectedItem());
        Timesheet timesheet = TimesheetDao.getInstance().getObjectById(timesheetId);
        TimesheetDao.getInstance().remove(timesheet);
        workRecordManagerTableView.refresh();
    }

    //metody do zarzadzania uzytkownikami dla admina

    public void banAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        int userRole = userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getUsersRolesId();
        if (userRole == 1 || userRole == 2 || userRole == 3){
            changeUserRole(userRole + 10);
            alert("Blokada konta", "Zablokowano konto użytkownika : " + userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getLogin(), "");
        } else if(userRole == 11 || userRole == 12 || userRole == 13){
            changeUserRole(userRole - 10);
            alert("Sciągnięto blokadę konta", "Odblokowano konto użytkownika : " + userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getLogin(), "");
        }
        else if(userRole == 21 || userRole == 22 || userRole == 23){
            userRole -= 10;
            changeUserRole(userRole);
            alert("Blokada konta", "Zablokowano konto użytkownika : " + userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getLogin(), "");
        }
    }

    public void forcePasswordChangeAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        int userRole = userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getUsersRolesId();
        if (userRole == 1 || userRole == 2 || userRole == 3){
            changeUserRole(userRole + 20);
            alert("Zmiana hasła", "Wymuszono zmianę hasła użytkownika : " + userList.get(userListTableView.getSelectionModel().getSelectedIndex()).getLogin(), "");
        } else if(userRole == 11 || userRole == 12 || userRole == 13){
            alert("Konflikt", "Wybrany użytkownik jest zablokowany", "");
        }
        else if(userRole == 21 || userRole == 22 || userRole == 23){
            alert("Konflikt", "Wybrany użytkownik już ma zleconą zmianę hasła", "");
        }
    }

    public void changeRoleAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        int userRole = userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getUsersRolesId();
        ChoiceDialog<String> dialog = new ChoiceDialog<>(roleList[0], roleList);
        dialog.setTitle("Wybór roli");
        dialog.setHeaderText("Uwaga!!! Zmiana roli spowoduje cofnięcie blokady oraz zlecenie zmiany hasła użytkownikowi.");
        dialog.setContentText("Wybierz nową rolę dla użytkownika " + userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getLogin() + " :");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            if(result.get().equals(roleList[0])) userRole = 1;
            else if(result.get().equals(roleList[1]))userRole = 2;
            else if(result.get().equals(roleList[2]))userRole = 3;
        }

        changeUserRole(userRole);
    }

    //metody do zarzadzania projektami dla admina

    public void addProjectAdminButtonHnd(ActionEvent actionEvent) {
        BPAdmin.setDisable(true);
        BPAdmin.setVisible(false);
        BPNewProject.setDisable(false);
        BPNewProject.setVisible(true);
    }

    public void editProjectAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        BPAdmin.setDisable(true);
        BPAdmin.setVisible(false);
        BPEditProject.setDisable(false);
        BPEditProject.setVisible(true);
        editProjectNameAdminTF.setText(projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getName());
        editProjectLocationAdminTF.setText(projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getLocation());
        project = ProjectDao.getInstance().getObjectById(projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getId());
    }

    public void deleteProjectAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        long projectId = projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getId();
        projectListAdmin.remove(projectsListTableView.getSelectionModel().getSelectedItem());
        Project project = ProjectDao.getInstance().getObjectById(projectId);
        ProjectDao.getInstance().remove(project);
        projectsListTableView.refresh();
        //filteredProjectAdmin();
    }

    public void newProjectOkAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        if(newProjectNameAdminTF.getText().isEmpty() || newProjectLocationAdminTF.getText().isEmpty()) alert("Brak wszystkich danych","Oba pola muszą być wypełnione","warning");
        else {
            Project project = new Project();
            project.setName(newProjectNameAdminTF.getText());
            project.setLocation(newProjectLocationAdminTF.getText());
            ProjectDao.getInstance().add(project);
            projectListAdmin.add(project);
            projectsListTableView.refresh();
            BPNewProject.setDisable(true);
            BPNewProject.setVisible(false);
            BPAdmin.setDisable(false);
            BPAdmin.setVisible(true);
            newProjectNameAdminTF.clear();
            newProjectLocationAdminTF.clear();
        }
    }

    public void newProjectCancelAdminButtonHnd(ActionEvent actionEvent) {
        BPNewProject.setDisable(true);
        BPNewProject.setVisible(false);
        BPAdmin.setDisable(false);
        BPAdmin.setVisible(true);
        newProjectNameAdminTF.clear();
        newProjectLocationAdminTF.clear();
    }

    public void editProjectOkAdminButtonHnd(ActionEvent actionEvent) throws SQLException {
        if(editProjectNameAdminTF.getText().isEmpty() || editProjectLocationAdminTF.getText().isEmpty()) alert("Brak wszystkich danych","Oba pola muszą być wypełnione","warning");
        else {
            project.setName(editProjectNameAdminTF.getText());
            project.setLocation(editProjectLocationAdminTF.getText());
            for (int i = 0; i < projectListAdmin.size(); i++) {
                if (projectListAdmin.get(i).getId() == project.getId()) {
                    projectListAdmin.get(i).setName(editProjectNameAdminTF.getText());
                    projectListAdmin.get(i).setLocation(editProjectLocationAdminTF.getText());
                    projectsListTableView.refresh();
                }
            }
            ProjectDao.getInstance().update(project);
            project = null;
            editProjectNameAdminTF.clear();
            editProjectLocationAdminTF.clear();
            BPEditProject.setDisable(true);
            BPEditProject.setVisible(false);
            BPAdmin.setDisable(false);
            BPAdmin.setVisible(true);
        }
    }

    public void editProjectCancelAdminButtonHnd(ActionEvent actionEvent) {
        BPEditProject.setDisable(true);
        BPEditProject.setVisible(false);
        BPAdmin.setDisable(false);
        BPAdmin.setVisible(true);
        project = null;
        editProjectNameAdminTF.clear();
        editProjectLocationAdminTF.clear();
    }

    //metody do przypisywania managerow i pracownikow dla admina

    public void setManagersAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        long projectId = projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getId();
        String managersList = ManagersAssignmentDialog.display(UserDao.getInstance().getAll(), projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getManagers());
        if(managersList!=null){
            List<Project> project = ProjectDao.getInstance().getAll();
            for (int i=0; i<project.size(); i++) {
                if(project.get(i).getId()==projectId) {
                    project.get(i).setManagers(managersList);
                    ProjectDao.getInstance().update(project.get(i));
                    for (int j = 0; j< projectListAdminSortedData.size(); j++){
                        if(projectListAdminSortedData.get(j).getId()==projectId){
                            projectListAdminSortedData.get(j).setManagers(managersList);
                            projectsListTableView.refresh();
                        }
                    }
                }
            }
        } else {
            List<Project> project = ProjectDao.getInstance().getAll();
            for (int i=0; i<project.size(); i++) {
                if(project.get(i).getId()==projectId) {
                    project.get(i).setManagers(null);
                    ProjectDao.getInstance().update(project.get(i));
                    for (int j = 0; j< projectListAdminSortedData.size(); j++){
                        if(projectListAdminSortedData.get(j).getId()==projectId){
                            projectListAdminSortedData.get(j).setManagers(null);
                            projectsListTableView.refresh();
                        }
                    }
                }
            }
        }
    }

    public void setWorkersAdminButtonHnd(ActionEvent actionEvent) throws SQLException{
        long projectId = projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getId();
        String workersList = WorkersAssignmentDialog.display(UserDao.getInstance().getAll(), projectListAdminSortedData.get(projectsListTableView.getSelectionModel().getSelectedIndex()).getWorkers());
        if(workersList!=null){
            List<Project> project = ProjectDao.getInstance().getAll();
            for (int i=0; i<project.size(); i++) {
                if(project.get(i).getId()==projectId) {
                    project.get(i).setWorkers(workersList);
                    ProjectDao.getInstance().update(project.get(i));
                    for (int j = 0; j< projectListAdminSortedData.size(); j++){
                        if(projectListAdminSortedData.get(j).getId()==projectId){
                            projectListAdminSortedData.get(j).setWorkers(workersList);
                            projectsListTableView.refresh();
                        }
                    }
                }
            }
        } else {
            List<Project> project = ProjectDao.getInstance().getAll();
            for (int i=0; i<project.size(); i++) {
                if(project.get(i).getId()==projectId) {
                    project.get(i).setWorkers(null);
                    ProjectDao.getInstance().update(project.get(i));
                    for (int j = 0; j< projectListAdminSortedData.size(); j++){
                        if(projectListAdminSortedData.get(j).getId()==projectId){
                            projectListAdminSortedData.get(j).setWorkers(null);
                            projectsListTableView.refresh();
                        }
                    }
                }
            }
        }
    }

    //metody do zmian w bazie

    private void changeUserRole(int userID) throws SQLException{
        userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).setUsersRolesId(userID);
        userListTableView.refresh();
        //filteredUsersAdmin();

        User user = UserDao.getInstance().getObjectById(userListSortedData.get(userListTableView.getSelectionModel().getSelectedIndex()).getId());

        user.setUsersRolesId(userID);
        UserDao.getInstance().update(user);

        List<UserRole> userRoleList = UserRoleDao.getInstance().getAll();

        for(int i = 0 ; i<userRoleList.size();i++){
            if(userRoleList.get(i).getUserID()==user.getId()){
                UserRole userRole = userRoleList.get(i);
                userRole.setRole(RoleDao.getInstance().getObjectById(userID));
                UserRoleDao.getInstance().update(userRole);
            }
        }
    }

    private void changePasswordDialog(String login) throws SQLException{
        String newPassword = PasswordChangeDialog.display();
        if(newPassword!=null){
            List<User> users = UserDao.getInstance().getAll();
            for (int i=0; i<users.size(); i++) {
                User user = users.get(i);
                if(user.getLogin().equals(login)) {
                    user.setPassword(MD5(newPassword));
                    UserDao.getInstance().update(user);

                    List<UserRole> userRoleList = UserRoleDao.getInstance().getAll();

                    if(user.getUsersRolesId()==21 || user.getUsersRolesId()==22 || user.getUsersRolesId()==23)
                        for(int j = 0 ; j<userRoleList.size();j++){
                            if(userRoleList.get(j).getUserID()==user.getId()){
                                user.setUsersRolesId(user.getUsersRolesId()-20);
                                UserDao.getInstance().update(user);

                                UserRole userRole = userRoleList.get(j);
                                userRole.setRole(RoleDao.getInstance().getObjectById(user.getUsersRolesId()));
                                UserRoleDao.getInstance().update(userRole);
                            }
                        }
                    alert("Zmieniono hasło", "Hasło do konto zostało zmienione", "");
                }
            }
        } else alert("Nie zmieniono hasła", "Anulowano formularz zmiany hasła", "");
    }

    //metody do wypelnania tabel danymi

    private void populateAdminTableViews() throws SQLException{
        userList = FXCollections.observableArrayList(UserDao.getInstance().getAll());

        loginAdminUserListColumn.setCellValueFactory(new PropertyValueFactory<User, String>("login"));
        nameAdminUserListColumn.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
        surnameAdminUserListColumn.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
        emailAdminUserListColumn.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
        roleAdminUserListColumn.setCellValueFactory(new PropertyValueFactory<User, String>("usersRolesId"));
        userListTableView.setItems(userList);
        userListTableView.getSelectionModel().select(0);

        filteredUsersAdmin();

        taskListAdmin = FXCollections.observableArrayList(TaskDao.getInstance().getAll());

        taskNameAdminTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
        taskCompletionAdminTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("completion"));
        /*taskEtaAdminTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, Date>("eta"));
        Callback<TableColumn<Task, Date>, TableCell<Task, Date>> etaAdminCellFactory = new Callback<TableColumn<Task, Date>, TableCell<Task, Date>>() {
            @Override
            public TableCell<Task, Date> call(TableColumn<Task, Date> param) {
                final TableCell<Task, Date> cell = new TableCell<Task, Date>() {
                    @Override
                    public void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                            java.util.Date date = getTableView().getItems().get(getIndex()).getEta();
                            setText(sdf.format(date));
                        }
                    }
                };
                return cell;
            }
        };
        taskEtaAdminTaskListColumn.setCellFactory(etaAdminCellFactory);*/
        taskEtaAdminTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("eta"));
        taskListAdminTableView.setItems(taskListAdmin);
        taskListAdminTableView.getSelectionModel().select(0);

        filteredTaskAdmin();

        projectListAdmin = FXCollections.observableArrayList(ProjectDao.getInstance().getAll());
        projectNameAdminProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
        projectLocationAdminProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("location"));
        projectManagersAdminProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("managers"));
        Callback<TableColumn<Project, String>, TableCell<Project, String>> managersAdminCellFactory = new Callback<TableColumn<Project, String>, TableCell<Project, String>>() {
            @Override
            public TableCell<Project, String> call(TableColumn<Project, String> param) {
                final TableCell<Project, String> cell = new TableCell<Project, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            if(getTableView().getItems().get(getIndex()).getManagers()==null) setText(null);
                            else if(getTableView().getItems().get(getIndex()).getManagers().contains(",")) setText(getTableView().getItems().get(getIndex()).getManagers().replace(",","\n"));
                            else setText(getTableView().getItems().get(getIndex()).getManagers());
                        }
                    }
                };
                return cell;
            }
        };
        projectManagersAdminProjectListColumn.setCellFactory(managersAdminCellFactory);
        projectWorkersAdminProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("workers"));
        Callback<TableColumn<Project, String>, TableCell<Project, String>> workersAdminCellFactory = new Callback<TableColumn<Project, String>, TableCell<Project, String>>() {
            @Override
            public TableCell<Project, String> call(TableColumn<Project, String> param) {
                final TableCell<Project, String> cell = new TableCell<Project, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            if(getTableView().getItems().get(getIndex()).getWorkers()==null) setText(null);
                            else if(getTableView().getItems().get(getIndex()).getWorkers().contains(",")) setText(getTableView().getItems().get(getIndex()).getWorkers().replace(",","\n"));
                            else setText(getTableView().getItems().get(getIndex()).getWorkers());
                        }
                    }
                };
                return cell;
            }
        };
        projectWorkersAdminProjectListColumn.setCellFactory(workersAdminCellFactory);
        projectsListTableView.setItems(projectListAdmin);
        projectsListTableView.getSelectionModel().select(0);

        filteredProjectAdmin();
    }

    private void populateManagerTableViews(String manager) throws SQLException{
        List<Project> projects = ProjectDao.getInstance().getAll();
        List<Project> currentManagerProjects = new ArrayList<Project>();
        for(int i=0;i<projects.size();i++){
            if(projects.get(i).getManagers()==null);
            else if(projects.get(i).getManagers().contains(",")){
                String[] workersList = projects.get(i).getManagers().split(",");
                for(int j=0;j<workersList.length;j++){
                    if(workersList[j].equals(manager))currentManagerProjects.add(projects.get(i));
                }
            }
            else if(projects.get(i).getManagers().equals(manager)) currentManagerProjects.add(projects.get(i));
        }

        projectListManager = FXCollections.observableArrayList(currentManagerProjects);
        projectNameManagerProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("name"));
        projectLocationManagerProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("location"));
        projectWorkersManagerProjectListColumn.setCellValueFactory(new PropertyValueFactory<Project, String>("workers"));
        Callback<TableColumn<Project, String>, TableCell<Project, String>> workersManagerCellFactory = new Callback<TableColumn<Project, String>, TableCell<Project, String>>() {
            @Override
            public TableCell<Project, String> call(TableColumn<Project, String> param) {
                final TableCell<Project, String> cell = new TableCell<Project, String>() {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            if(getTableView().getItems().get(getIndex()).getWorkers()==null) setText(null);
                            else if(getTableView().getItems().get(getIndex()).getWorkers().contains(",")) setText(getTableView().getItems().get(getIndex()).getWorkers().replace(",","\n"));
                            else setText(getTableView().getItems().get(getIndex()).getWorkers());
                        }
                    }
                };
                return cell;
            }
        };
        projectWorkersManagerProjectListColumn.setCellFactory(workersManagerCellFactory);
        projectsListManagerTableView.setItems(projectListManager);
        projectsListManagerTableView.getSelectionModel().select(0);

        filteredProjectManager();

        taskListManager = FXCollections.observableArrayList(TaskDao.getInstance().getAll());

        taskNameManagerTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
        taskCompletionManagerTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("completion"));
        /*taskEtaManagerTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, Date>("eta"));
        Callback<TableColumn<Task, Date>, TableCell<Task, Date>> etaManagerCellFactory = new Callback<TableColumn<Task, Date>, TableCell<Task, Date>>() {
            @Override
            public TableCell<Task, Date> call(TableColumn<Task, Date> param) {
                final TableCell<Task, Date> cell = new TableCell<Task, Date>() {
                    @Override
                    public void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                            java.util.Date date = getTableView().getItems().get(getIndex()).getEta();
                            setText(sdf.format(date));
                        }
                    }
                };
                return cell;
            }
        };
        taskEtaManagerTaskListColumn.setCellFactory(etaManagerCellFactory);*/
        taskEtaManagerTaskListColumn.setCellValueFactory(new PropertyValueFactory<Task, String>("eta"));
        taskListManagerTableView.setItems(taskListManager);
        taskListManagerTableView.getSelectionModel().select(0);

        List<Timesheet> timesheetsManager = TimesheetDao.getInstance().getAll();
        List<Timesheet> acceptedTimesheetsManager = new ArrayList<Timesheet>();
        for(int i=0;i<timesheetsManager.size();i++){
            if(timesheetsManager.get(i).getStatus()==1 || timesheetsManager.get(i).getStatus()==2) acceptedTimesheetsManager.add(timesheetsManager.get(i));
        }

        filteredTaskManager();

        timesheetListManager = FXCollections.observableArrayList(acceptedTimesheetsManager);

        dateTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, Date>("date_submitted"));
        Callback<TableColumn<Timesheet, Date>, TableCell<Timesheet, Date>> dateTimesheetManagerCellFactory = new Callback<TableColumn<Timesheet, Date>, TableCell<Timesheet, Date>>() {
            @Override
            public TableCell<Timesheet, Date> call(TableColumn<Timesheet, Date> param) {
                final TableCell<Timesheet, Date> cell = new TableCell<Timesheet, Date>() {
                    @Override
                    public void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                            java.util.Date date = getTableView().getItems().get(getIndex()).getDateSubmitted();
                            setText(sdf.format(date));
                        }
                    }
                };
                return cell;
            }
        };
        dateTimesheetManagerColumn.setCellFactory(dateTimesheetManagerCellFactory);
        timeTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("time"));
        projectTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("projectName"));
        taskTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("taskName"));
        commentTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("comment"));
        statusTimesheetManagerColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("status"));
        Callback<TableColumn<Timesheet, Integer>, TableCell<Timesheet, Integer>> statusTimesheetManagerCellFactory = new Callback<TableColumn<Timesheet, Integer>, TableCell<Timesheet, Integer>>() {
            @Override
            public TableCell<Timesheet, Integer> call(TableColumn<Timesheet, Integer> param) {
                final TableCell<Timesheet, Integer> cell = new TableCell<Timesheet, Integer>() {
                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            if(getTableView().getItems().get(getIndex()).getStatus()==1)setText("Nowy wpis");
                            else if(getTableView().getItems().get(getIndex()).getStatus()==2)setText("Zmodyfikowany wpis");
                        }
                    }
                };
                return cell;
            }
        };
        statusTimesheetManagerColumn.setCellFactory(statusTimesheetManagerCellFactory);
        workRecordManagerTableView.setItems(timesheetListManager);
        workRecordManagerTableView.getSelectionModel().select(0);

        filteredTimesheetManager();
    }

    private void populateUserTableViews() throws SQLException{
        //List<Timesheet> timesheetsUser = TimesheetDao.getInstance().getAll();
        //List<Timesheet> acceptedTimesheetsUser = new ArrayList<Timesheet>();
        //for(int i=0;i<timesheetsUser.size();i++){
        //    if(timesheetsUser.get(i).getStatus()==0) acceptedTimesheetsUser.add(timesheetsUser.get(i));
        //}

        //timesheetListUser = FXCollections.observableArrayList(acceptedTimesheetsUser);
        timesheetListUser = FXCollections.observableArrayList(TimesheetDao.getInstance().getAll());

        dateTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, Date>("date_submitted"));
        Callback<TableColumn<Timesheet, Date>, TableCell<Timesheet, Date>> dateTimesheetUserCellFactory = new Callback<TableColumn<Timesheet, Date>, TableCell<Timesheet, Date>>() {
            @Override
            public TableCell<Timesheet, Date> call(TableColumn<Timesheet, Date> param) {
                final TableCell<Timesheet, Date> cell = new TableCell<Timesheet, Date>() {
                    @Override
                    public void updateItem(Date item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                            java.util.Date date = getTableView().getItems().get(getIndex()).getDateSubmitted();
                            setText(sdf.format(date));
                        }
                    }
                };
                return cell;
            }
        };
        dateTimesheetUserColumn.setCellFactory(dateTimesheetUserCellFactory);
        timeTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("time"));
        projectTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("projectName"));
        taskTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("taskName"));
        commentTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("comment"));
        statusTimesheetUserColumn.setCellValueFactory(new PropertyValueFactory<Timesheet, String>("status"));
        Callback<TableColumn<Timesheet, Integer>, TableCell<Timesheet, Integer>> statusTimesheetUserCellFactory = new Callback<TableColumn<Timesheet, Integer>, TableCell<Timesheet, Integer>>() {
            @Override
            public TableCell<Timesheet, Integer> call(TableColumn<Timesheet, Integer> param) {
                final TableCell<Timesheet, Integer> cell = new TableCell<Timesheet, Integer>() {
                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            setGraphic(null);
                            if(getTableView().getItems().get(getIndex()).getStatus()==0)setText("Zaakceptowano");
                            else setText("Wpis czeka na akceptację");
                        }
                    }
                };
                return cell;
            }
        };
        statusTimesheetUserColumn.setCellFactory(statusTimesheetUserCellFactory);
        workRecordUserTableView.setItems(timesheetListUser);
        workRecordUserTableView.getSelectionModel().select(0);

        filterTimesheetUser();
    }

    //metody do filtracji danych w tabeli

    private void filterTimesheetUser(){
        FilteredList<Timesheet> filteredTimesheetUser = new FilteredList<>(timesheetListUser, p -> true);

        filterTimesheetUser.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredTimesheetUser.setPredicate(timesheet -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (timesheet.getProjectName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getTaskName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getDateSubmitted().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getComment().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }  else if (Integer.toString(timesheet.getTime()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        timesheetListUserSortedData = new SortedList<>(filteredTimesheetUser);
        timesheetListUserSortedData.comparatorProperty().bind(workRecordUserTableView.comparatorProperty());
        workRecordUserTableView.setItems(timesheetListUserSortedData);
    }

    private void filteredTimesheetManager(){
        FilteredList<Timesheet> filteredTimesheetManager = new FilteredList<>(timesheetListManager, p -> true);

        filterTimesheetManager.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredTimesheetManager.setPredicate(timesheet -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (timesheet.getProjectName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getTaskName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getDateSubmitted().toString().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (timesheet.getComment().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }  else if (Integer.toString(timesheet.getTime()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        timesheetListManagerSortedData = new SortedList<>(filteredTimesheetManager);
        timesheetListManagerSortedData.comparatorProperty().bind(workRecordManagerTableView.comparatorProperty());
        workRecordManagerTableView.setItems(timesheetListManagerSortedData);
    }

    private void filteredProjectManager(){
        FilteredList<Project> filteredProjectListManager = new FilteredList<>(projectListManager, p -> true);

        filterProjectManager.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredProjectListManager.setPredicate(project -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (project.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (project.getLocation().contains(lowerCaseFilter)) {
                    return true;
                } else if (project.getWorkers().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        projectListManagerSortedData = new SortedList<>(filteredProjectListManager);
        projectListManagerSortedData.comparatorProperty().bind(projectsListManagerTableView.comparatorProperty());
        projectsListManagerTableView.setItems(projectListManagerSortedData);
    }

    private void filteredProjectAdmin(){
        FilteredList<Project> filteredProjectListAdmin = new FilteredList<>(projectListAdmin, p -> true);

        filterProjectAdmin.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredProjectListAdmin.setPredicate(project -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (project.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (project.getLocation().contains(lowerCaseFilter)) {
                    return true;
                } else if (project.getManagers().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (project.getWorkers().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        projectListAdminSortedData = new SortedList<>(filteredProjectListAdmin);
        projectListAdminSortedData.comparatorProperty().bind(projectsListTableView.comparatorProperty());
        projectsListTableView.setItems(projectListAdminSortedData);
    }

    private void filteredTaskManager(){
        FilteredList<Task> filteredTaskListManager = new FilteredList<>(taskListManager, p -> true);

        filterTaskManager.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredTaskListManager.setPredicate(task -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (task.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (Integer.toString(task.getCompletion()).contains(lowerCaseFilter)) {
                    return true;
                } else if (Integer.toString(task.getEta()).contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        taskListManagerSortedData = new SortedList<>(filteredTaskListManager);
        taskListManagerSortedData.comparatorProperty().bind(taskListManagerTableView.comparatorProperty());
        taskListManagerTableView.setItems(taskListManagerSortedData);
    }

    private void filteredTaskAdmin(){
        FilteredList<Task> filteredTaskListAdmin = new FilteredList<>(taskListAdmin, p -> true);

        filterTaskAdmin.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredTaskListAdmin.setPredicate(task -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (task.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (Integer.toString(task.getCompletion()).contains(lowerCaseFilter)) {
                    return true;
                } else if (Integer.toString(task.getEta()).contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        taskListAdminSortedData = new SortedList<>(filteredTaskListAdmin);
        taskListAdminSortedData.comparatorProperty().bind(taskListAdminTableView.comparatorProperty());
        taskListAdminTableView.setItems(taskListAdminSortedData);
    }

    private void filteredUsersAdmin(){
        FilteredList<User> filteredUserListAdmin = new FilteredList<>(userList, p -> true);

        filterUsersAdmin.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredUserListAdmin.setPredicate(user -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (user.getLogin().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (user.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (user.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (user.getEmail().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (Integer.toString(user.getUsersRolesId()).contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        userListSortedData = new SortedList<>(filteredUserListAdmin);
        userListSortedData.comparatorProperty().bind(userListTableView.comparatorProperty());
        userListTableView.setItems(userListSortedData);
    }

    private void alert(String title, String message, String type) {
        Alert alert;
        if (type.equals("warning")) alert = new Alert(Alert.AlertType.WARNING);
        else if (type.equals("error")) alert = new Alert(Alert.AlertType.ERROR);
        else alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    private String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    private boolean isNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }


    /*private void debugPanelSwitch(int value) {
        if (debugPanelVal > 5) debugPanelVal = 0;
        if (debugPanelVal == 0) {
            BPNewAccount.setDisable(true);
            BPNewAccount.setVisible(false);
            BPLogin.setDisable(false);
            BPLogin.setVisible(true);
        } else if (debugPanelVal == 1) {
            BPLogin.setDisable(true);
            BPLogin.setVisible(false);
            BPForgottenPassword.setDisable(false);
            BPForgottenPassword.setVisible(true);
        } else if (debugPanelVal == 2) {
            BPForgottenPassword.setDisable(true);
            BPForgottenPassword.setVisible(false);
            BPUser.setDisable(false);
            BPUser.setVisible(true);
        } else if (debugPanelVal == 3) {
            BPUser.setDisable(true);
            BPUser.setVisible(false);
            BPManager.setDisable(false);
            BPManager.setVisible(true);
        } else if (debugPanelVal == 4) {
            BPManager.setDisable(true);
            BPManager.setVisible(false);
            BPAdmin.setDisable(false);
            BPAdmin.setVisible(true);
        } else if (debugPanelVal == 5) {
            BPAdmin.setDisable(true);
            BPAdmin.setVisible(false);
            BPNewAccount.setDisable(false);
            BPNewAccount.setVisible(true);
        }
        debugPanelVal++;
    }*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginController = new LoginController();
        BPLogin.setDisable(false);
        BPLogin.setVisible(true);

        PFPassword.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke)
            {
                if (ke.getCode().equals(KeyCode.ENTER))
                {
                    OKLoginButton.fire();
                }
            }
        });

        //dane do comboboxa

        questionsNewAccountCB.getItems().addAll(helpQuestions);

        //debugPanelVal = 0;

        /*APMain.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                if (t.isMiddleButtonDown()) {
                    debugPanelSwitch(debugPanelVal);
                }
            }
        });*/
    }
}
