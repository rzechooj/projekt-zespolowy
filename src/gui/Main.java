package gui;

import dao.UserDao;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Project;
import model.Timesheet;
import model.User;

import java.sql.Date;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("gui.fxml"));
        primaryStage.setTitle("Projekt PZ i PZUMZ");
        primaryStage.setScene(new Scene(root, 640, 480));

//        TODO TESTY, PRZYKŁADY - do usunięcia
        Project project = new Project();
        project.setName("Projekt spider");
        project.setLocation("glywyce");

     //   ProjectDao.getInstance().add(project);

        User user = UserDao.getInstance().getObjectById(4);

        Timesheet timesheet = new Timesheet();
        timesheet.setUser(user);
        timesheet.setProject(project);
        //timesheet.setTimeFrom(new Time(1558031056));
        timesheet.setTime(1);
        timesheet.setDateSubmitted(new Date(1558031056));

//        TimesheetDao.getInstance().add(timesheet);

    //    List<Timesheet> list = TimesheetDao.getInstance().getAll();
     //   Timesheet testTimeSheet = TimesheetDao.getInstance().getObjectById(1);
      // User spider = UserDao.getInstance().getObjectById(5);
     //  WorkerProject wp = new WorkerProject("Projekt śmierci", new LoginController(),spider);
        //wp.getTimesheetsForProject(spider.getLogin(),wp.getProjectName());
    //    wp.addToTimesheet(new Time(7,0,0),new Time(15,0,0));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
