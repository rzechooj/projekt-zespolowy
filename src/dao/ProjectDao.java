package dao;

import model.Project;

import java.sql.SQLException;
import java.util.List;

public class ProjectDao extends BaseDao<Project> {

    private static ProjectDao instance;

    private ProjectDao() throws SQLException {
        super(Project.class);
    }

    public static ProjectDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new ProjectDao();
        }

        return instance;
    }

    public Project getProjectByName(String projectName) throws SQLException {
        List<Project> projects = dao.queryBuilder().where().eq("name", projectName).query();
        if(projects.size()>1) System.out.println("multiple projects with this name");
        return (projects.size()==1) ? projects.get(0) : null;
    }
}
