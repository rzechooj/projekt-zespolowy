package dao;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import model.Timesheet;
import model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TimesheetDao extends BaseDao<Timesheet> {

    private static TimesheetDao instance;

    private TimesheetDao() throws SQLException {
        super(Timesheet.class);
    }

    public static TimesheetDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new TimesheetDao();
        }

        return instance;
    }

    @Override
    public Timesheet getObjectById(long id) throws SQLException {
        Timesheet timesheet = super.getObjectById(id);

        timesheet.setUser(UserDao.getInstance().getObjectById(timesheet.getUser().getId()));
        timesheet.setProject(ProjectDao.getInstance().getObjectById(timesheet.getProject().getId()));

        return timesheet;
    }

    @Override
    public List<Timesheet> getAll() throws SQLException {
        List<Timesheet> list = new ArrayList<>();

        for (Timesheet timesheet : super.getAll()) {
            timesheet.setUser(UserDao.getInstance().getObjectById(timesheet.getUser().getId()));
            timesheet.setProject(ProjectDao.getInstance().getObjectById(timesheet.getProject().getId()));
            list.add(timesheet);
        }

        return list;
    }

    public List<Timesheet> getTimesheetsForProjectByName(String projectName, User u) throws SQLException {
        Long user_id = u.getId();
        Long id = ProjectDao.getInstance().getProjectByName(projectName).getId();
        if (id == null){
        System.out.println("No project with such name found");
        return null;
        }
        QueryBuilder<Timesheet, String> qb = dao.queryBuilder();


        Where where = qb.where();
        where.eq("user_id", user_id.toString());
        where.and();
        where.eq("project_id", id.toString());
        PreparedQuery<Timesheet> pq = qb.prepare();
        List<Timesheet> timesheets = dao.query(pq);
        return (timesheets.size()>0) ? timesheets : null;
    }
}