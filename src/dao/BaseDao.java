package dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import model.BaseModel;

import java.sql.SQLException;
import java.util.List;

abstract class BaseDao<M extends BaseModel> {
    Dao<M, String> dao;

    BaseDao(Class<M> model) throws SQLException {
        dao = DaoManager.createDao(this.connectDataBase(), model);
    }

    ConnectionSource connectDataBase() {
        try {
            String databaseUrl = "jdbc:sqlite:./db/base.db";
            return new JdbcConnectionSource(databaseUrl);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int add(M m) throws SQLException {
        return this.dao.create(m);
    }

    public int update(M m) throws SQLException {
        return this.dao.update(m);
    }

    public int remove(M m) throws SQLException {
        return this.dao.delete(m);
    }

    public List<M> getAll() throws SQLException {
        return this.dao.queryForAll();
    }

    public M getObjectById(long id) throws SQLException {
        return this.dao.queryForId(String.valueOf(id));
    }

    public boolean checkIfExist(M model) throws SQLException {
        QueryBuilder<M, String> qb = dao.queryBuilder();
        qb.setCountOf(true);

        Where where = qb.where();
        where.eq(BaseModel.COLUMN_ID, model.getId());

        return dao.countOf(qb.prepare()) > 0;
    }
}
