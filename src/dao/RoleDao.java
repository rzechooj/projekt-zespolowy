package dao;

import model.Role;
import model.User;

import java.sql.SQLException;
import java.util.List;

public class RoleDao extends BaseDao<Role> {
    private static RoleDao instance;

    public static RoleDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new RoleDao();
        }

        return instance;
    }

    private RoleDao() throws SQLException {
        super(Role.class);
    }

    public List<Role> getRolesForUserByID(long id) throws SQLException {
        return dao.queryBuilder().where().eq(Role.COLUMN_ID, id).query();
    }

    public List<Role> getRolesForUser(User user) throws SQLException {
        return dao.queryBuilder().where().eq(User.COLUMN_ID, user.getId()).query();
    }
}
