package dao;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import model.Role;
import model.User;
import model.UserRole;

import java.sql.SQLException;
import java.util.List;

public class UserRoleDao extends BaseDao<UserRole> {
    private static UserRoleDao instance;

    public static UserRoleDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new UserRoleDao();
        }

        return instance;
    }

    private UserRoleDao() throws SQLException {
        super(UserRole.class);
    }

    public List<UserRole> getUserRole(User user) throws SQLException {
        UserRole userRole = new UserRole();
        userRole.setUser(user);

        QueryBuilder<UserRole, String> qb = dao.queryBuilder();
        Where where = qb.where();
        where.eq("user_id", user.getId());
        PreparedQuery<UserRole> pq = qb.prepare();

        return dao.query(pq);
    }

    public boolean setUserRole(User user, Role role) throws SQLException {
        //TODO Sprawdzić wcześniej czy istnieje takie powiązanie już
        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);

        return this.dao.create(userRole) > 0;
    }
}
