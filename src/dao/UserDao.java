package dao;

import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import model.User;

import java.sql.SQLException;
import java.util.List;

public class UserDao extends BaseDao<User> {

    private static UserDao instance;

    private UserDao() throws SQLException {
        super(User.class);
    }

    public static UserDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new UserDao();
        }

        return instance;
    }

    public List<User> getUserIdFromDB(User user) throws SQLException {
        return dao.queryBuilder().where().eq(User.COLUMN_LOGIN, user.getLogin()).query();
    }

    public boolean findUser(String login, String password) throws SQLException {
        QueryBuilder<User, String> qb = dao.queryBuilder();
        qb.setCountOf(true);

        Where where = qb.where();
        where.eq(User.COLUMN_LOGIN, login);
        where.and();
        where.eq(User.COLUMN_PASSWORD, password);
        System.out.println(dao.countOf(qb.prepare()) > 0);
        return dao.countOf(qb.prepare()) > 0;
    }

    public boolean checkUser(String login) throws SQLException {
        List<User> users = dao.queryBuilder().where().eq(User.COLUMN_LOGIN, login).query();
        if(users.size()==0) return false;
        else return true;
    }

    public String retrievePasswordFromDB(String login) throws SQLException {
        List<User> users = dao.queryBuilder().where().eq(User.COLUMN_LOGIN, login).query();
        return users.get(0).getPassword();
    }

    public int retriveHelpQuestionFromDB(String login) throws SQLException{
        List<User> users = dao.queryBuilder().where().eq(User.COLUMN_LOGIN, login).query();
        return users.get(0).getHelpQuestion();
    }

    public String retrieveHelpAnswerFromDB(String login) throws SQLException {
        List<User> users = dao.queryBuilder().where().eq(User.COLUMN_LOGIN, login).query();
        return users.get(0).getHelpAnswer();
    }
}
