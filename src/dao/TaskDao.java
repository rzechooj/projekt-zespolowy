package dao;

import model.Task;

import java.sql.SQLException;

public class TaskDao extends BaseDao<Task> {
    private static TaskDao instance;

    public static TaskDao getInstance() throws SQLException {
        if (instance == null) {
            instance = new TaskDao();
        }

        return instance;
    }

    private TaskDao() throws SQLException {
        super(Task.class);
    }
}
