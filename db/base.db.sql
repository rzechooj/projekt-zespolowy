BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "tasks" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"name"	TEXT NOT NULL,
	"completion"	INTEGER NOT NULL,
	"eta"	INTEGER
);
CREATE TABLE IF NOT EXISTS "timesheet" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"user_id"	int,
	"project_id"	int,
	"task_id"	int,
	"date_submitted"	date NOT NULL,
	"time"	INTEGER,
	"project_name"	TEXT,
	"task_name"	TEXT,
	"comment"	TEXT,
	"status"	INTEGER DEFAULT 1,
	"old_id"	INTEGER,
	FOREIGN KEY("user_id") REFERENCES "users",
	FOREIGN KEY("task_id") REFERENCES "tasks",
	FOREIGN KEY("project_id") REFERENCES "projects"
);
CREATE TABLE IF NOT EXISTS "projects" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"name"	TEXT NOT NULL,
	"location"	TEXT,
	"managers"	TEXT,
	"workers"	INTEGER
);
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"login"	TEXT NOT NULL,
	"password"	TEXT NOT NULL,
	"first_name"	TEXT NOT NULL,
	"last_name"	TEXT NOT NULL,
	"e_mail"	TEXT NOT NULL,
	"users_roles_id"	int,
	"help_question"	int NOT NULL,
	"help_answer"	TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "users_roles" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"user_id"	int,
	"role_id"	int,
	FOREIGN KEY("user_id") REFERENCES "users",
	FOREIGN KEY("role_id") REFERENCES "roles"
);
CREATE TABLE IF NOT EXISTS "roles" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"name"	TEXT NOT NULL
);
INSERT INTO "tasks" VALUES (3,'ggg',25,20);
INSERT INTO "tasks" VALUES (4,'zad1',13,58);
INSERT INTO "tasks" VALUES (9,'asdf',13,10);
INSERT INTO "timesheet" VALUES (1,7,12,3,'2019-05-28',4,'Projekt spider','ggg','brak',0,NULL);
INSERT INTO "timesheet" VALUES (2,7,12,3,'2019-05-27',4,'Projekt spider','ggg','brak',0,0);
INSERT INTO "timesheet" VALUES (6,7,1,3,'2019-05-29',8,'Projekt śmierci','ggg','Brak',0,NULL);
INSERT INTO "timesheet" VALUES (7,7,16,3,'2019-05-29',0,'zzz','ggg','Brak',0,0);
INSERT INTO "timesheet" VALUES (34,7,16,3,'2019-05-30',1,'zzz','ggg','test13',0,32);
INSERT INTO "timesheet" VALUES (35,7,1,3,'2019-06-03',3,'Projekt śmierci','ggg','heheh',0,0);
INSERT INTO "timesheet" VALUES (36,7,1,3,'2019-06-03',1,'Projekt śmierci','ggg','12',0,0);
INSERT INTO "timesheet" VALUES (38,7,1,9,'2019-06-03',1,'Projekt śmierci','asdf','test',0,0);
INSERT INTO "projects" VALUES (1,'Projekt śmierci','Przemysłowa Street','aaa,bbb,ccc,mod','user');
INSERT INTO "users" VALUES (5,'spider','f1a81d782dea6a19bdca383bffe68452','dd','dd','dd',2,1,'ffsf');
INSERT INTO "users" VALUES (7,'user','ee11cbb19052e40b07aac0ca060c23ee','user','user','user@user.user',1,0,'user');
INSERT INTO "users" VALUES (9,'admin','21232f297a57a5a743894a0e4a801fc3','admin','admin','admin@admin.admin',3,2,'admin');

INSERT INTO "users_roles" VALUES (10,5,2);
INSERT INTO "users_roles" VALUES (11,7,1);
INSERT INTO "users_roles" VALUES (13,9,3);
INSERT INTO "roles" VALUES (1,'1');
INSERT INTO "roles" VALUES (2,'2');
INSERT INTO "roles" VALUES (3,'3');

CREATE UNIQUE INDEX IF NOT EXISTS "table_name_e_mail_uindex" ON "users" (
	"e_mail"
);
CREATE UNIQUE INDEX IF NOT EXISTS "table_name_login_uindex" ON "users" (
	"login"
);
COMMIT;
